// beforeEach(() => {
//     cy.timeout(10000)
// })
describe("Client's login", () => {
    it('Successfully login to "Active" company', () => {
        cy.intercept('/api/auth/current-company').as('getCompany')

        cy.visit('/login')

        // Check that "Phone number" field is present
        cy.get('input[name=telephone]').should('exist').type(447336979051)

        // Check that "double tick" icon is appeared after entering the right phone number
        cy.get('.v-icon').should('exist')

        // Click on the "Next Step button"
        cy.contains('button', 'Next step').click()

        // Check that "Password" field is present on the next "Authorization" step
        cy.get('input[type=password]', { timeout: 30000 }).should('exist').type('1QAZ2wsx!!')

        // Click on the "Next Step" button
        cy.contains('button', 'Next step').click({ multiple: true })

        // Client see the company list
        cy.get('.company-list .company-list__item', { timeout: 10000 }).should('have.length.greaterThan', 2);
        cy.contains('button', 'Sign in').should('be.disabled');
    
        // Client click on the active
        cy.get('.company-list > :nth-child(2)').click({ force: true });
        cy.get('.company-list > :nth-child(2)').trigger('click');
        cy.get('#app > div > main > div > div > div.auth-layout__right-col > div > form > div > div > div.row.flex-grow-0.pt-0 > div > div > ul > li:nth-child(2)').then($el => {
            $el[0].click(); // Click the DOM element directly
          });
          
        // cy.get('#app > div > main > div > div > div.auth-layout__right-col > div > form > div > div > div.row.flex-grow-0.pt-0 > div > div > ul > li:nth-child(2) > div.d-flex.align-center > div.v-avatar.mr-3.primary').click();
        // cy.get('#app > div > main > div > div > div.auth-layout__right-col > div > form > div > div > div.row.flex-grow-0.pt-0 > div > div > ul > li:nth-child(2) > div.d-flex.align-center > div.v-avatar.mr-3.primary > div').click();
        // cy.contains(' Hintz, Fisher and McLaughlin ').click()

        // cy.get(':nth-child(2) > .align-center > .v-avatar').click({ force: true });
        // Then check for highlighting
        

        // // After clicking on the company - is highlited
        // cy.get('.company-list .company-list__item', { timeout: 30000 }).eq(1).should('be.visible')

        // // After clicking on the company - "v" icon is appeared
        cy.get('.current > :nth-child(2) > .v-icon').should('exist');

        // // "Sign in" button is active
        // cy.contains('button', 'Sign in', { timeout: 10000 }).should('be.enabled').click();

        // // cy.wait(10000);
        // cy.wait('@getCompany', { timeout: 10000 }).its('response.statusCode').should('eq', 200)

        // cy.wait(15000);

        // // cy.get('.v-avatar').should('exist')
        // cy.url().should('eq', 'https://business.stage.dzing.com/accounts')

    });

    // it.skip('Successfuly login to the "Under')
})