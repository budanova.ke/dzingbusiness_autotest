// beforeEach(() => {
//     cy.timeout(10000)
// })
describe("Client's login", () => {
    it('Un successfully login => Click on the "X" icon', () => {
        cy.visit('/login')

        // Check that "Phone number" field is present
        cy.get('input[name=telephone]').should('exist')
            .type(447336979051)

        // Check that "double tick" icon is appeared after entering the right phone number
        cy.get('.v-icon').should('exist')

        // Click on the "Next Step button"
        cy.contains('button', 'Next step').click()

        // Check that "Password" field is present on the next "Authorization" step
        cy.get('.v-input__control').should('exist')
        cy.get('input[type=password]').type('Passjsgdgsaword1245!')

        // Click on the "Next Step" button
        cy.contains('button', 'Next step').click({ multiple: true })

        // Error pop-up is appeared
        cy.get('.dialog-container').should('exist', { timeout: 30000 });
        cy.get('.dialog-content > .mb-8').should('contain', 'Error')

        // The "The password you entered is incorrect" message is visible
        cy.get('.dialog-content > .mb-4').should('contain', 'The password you entered is incorrect')

        // Click on the "X" icon
        cy.get('.dialog-header > .v-btn > .v-btn__content > .v-icon').should('exist').click()

        // Error pop-up is closed
        cy.get('.dialog-container').should('not.exist', { timeout: 30000 });        
    })
    it('Un successfully login => Click on the "OK" button', () => {
        cy.visit('/login')

        // Check that "Phone number" field is present
        cy.get('input[name=telephone]').should('exist')
            .type(447336979051)

        // Check that "double tick" icon is appeared after entering the right phone number
        cy.get('.v-icon').should('exist')

        // Click on the "Next Step button"
        cy.contains('button', 'Next step').click()

        // Check that "Password" field is present on the next "Authorization" step
        cy.get('.v-input__control').should('exist')
        cy.get('input[type=password]').type('Passjsgdgsaword1245!')

        // Click on the "Next Step" button
        cy.contains('button', 'Next step').click({ multiple: true })

        // Error pop-up is appeared
        cy.get('.dialog-container').should('exist', { timeout: 30000 });
        cy.get('.dialog-content > .mb-8').should('contain', 'Error')

        // The "The password you entered is incorrect" message is visible
        cy.get('.dialog-content > .mb-4').should('contain', 'The password you entered is incorrect')

        // Click on the "OK" button
        cy.get('.dialog-content > .v-btn--block > .v-btn__content').should('exist').click()

        // Error pop-up is closed
        cy.get('.dialog-container').should('not.exist', { timeout: 30000 });        
    }) 
    it('Successfully login', () => {
        cy.visit('/login')

        // Check that "Phone number" field is present
        cy.get('input[name=telephone]')
            .should('exist')
            .type(447336979051)

        // Check that "double tick" icon is appeared after entering the right phone number
        cy.get('.v-icon').should('exist')

        // Click on the "Next Step button"
        cy.contains('button', 'Next step').click()

        // Check that "Password" field is present on the next "Authorization" step
        cy.get('.v-input__control', { timeout: 30000 }).should('exist')
        cy.get('input[type=password]').type('1QAZ2wsx!!')

        // Click on the "Next Step" button
        cy.contains('button', 'Next step').click({ multiple: true })

        // Client see the company list
        cy.get('.company-list', { timeout: 30000 }).should('exist');
    })
})