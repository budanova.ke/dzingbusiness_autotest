const { defineConfig } = require("cypress");

module.exports = defineConfig({
  
  e2e: {
    setupNodeEvents(on, config) {
      // implement node event listeners here
    },
    baseUrl: 'https://business.stage.dzing.com/',
    viewportWidth: 1280, // Set the width of the viewport here
    viewportHeight: 720, // Set the height of the viewport here
  },
});